import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by herickmota on 03/10/17.
 */
public class LinkNewUserTest {

    private ChromeDriver driver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "/Users/herickmota/IdeaProjects/alura-selenium/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/usuarios");
    }

    @Test
    public void checkNewUserLink() {

        WebElement linknewUser = driver.findElement(By.linkText("Novo Usuário"));
        linknewUser.click();
    }

    @After
    public void finishTest() {
        driver.quit();
    }
}
