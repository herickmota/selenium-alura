import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

/**
 * Created by herickmota on 03/10/17.
 */
public class UserNameFailTest {

    private ChromeDriver driver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "/Users/herickmota/IdeaProjects/alura-selenium/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/usuarios/new");
    }

    @Test
    public void emptyUserNameTest() {

        //Finds the element in the page and saves it in the variables
        WebElement name = driver.findElement(By.name("usuario.nome"));
        WebElement email = driver.findElement(By.name("usuario.email"));

        //type the parameters in the element found above
        name.sendKeys("");
        email.sendKeys("herick.mota@sovos.com");

        //find the save button element
        WebElement saveBtn = driver.findElement(By.id("btnSalvar"));
        //clicks on the save button element
        saveBtn.click();

        boolean validaMsg = driver.getPageSource().contains("Nome obrigatorio!");

        assertTrue(validaMsg);
    }

    @After
    public void finishTest() {
        driver.quit();
    }
}
