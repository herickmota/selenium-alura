package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by herickmota on 03/10/17.
 */
public class RegisterPage {

    private WebDriver driver;

    public RegisterPage(WebDriver driver) {

        this.driver = driver;
    }

    public void register(String name, String email) {
        WebElement lb_name = driver.findElement(By.name("usuario.nome"));
        WebElement lb_email = driver.findElement(By.name("usuario.email"));

        lb_name.sendKeys(name);
        lb_email.sendKeys(email);

        lb_name.submit();

    }
}
