package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by herickmota on 03/10/17.
 */
public class UserPage {

    private WebDriver driver;

    public UserPage(WebDriver driver) {
        this.driver = driver;
    }

    public void visit() {
        driver.get("http://localhost:8080/usuarios");
    }

    public RegisterPage newUser() {
        driver.findElement(By.linkText("Novo Usuário")).click();
        return new RegisterPage(driver);
    }

    public boolean validateList(String name, String email) {
        return driver.getPageSource().contains(name) &&
                driver.getPageSource().contains(email);
    }
}
