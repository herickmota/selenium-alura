
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.UserPage;

import static org.junit.Assert.assertTrue;


/**
 * Created by herickmota on 03/10/17.
 */
public class UsersSystemTest {
    private ChromeDriver driver;
    private UserPage user;

    @Before
    public void setup() {

        System.setProperty("webdriver.chrome.driver", "/Users/herickmota/IdeaProjects/alura-selenium/drivers/chromedriver");
        this.driver = new ChromeDriver();
        this.user = new UserPage(driver);

    }

    @Test
    public void addUserToSystem() {

        user.visit();
        user.newUser().register("Herick Mota", "herick.mota@sovos.com");

        assertTrue(user.validateList("Herick Mota", "herick.mota@sovos.com"));

    }

    @After
    public void finishTest() {
        driver.quit();
    }
}
