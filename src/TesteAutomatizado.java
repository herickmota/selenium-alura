import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.openqa.selenium.By.name;

/**
 * Created by herickmota on 07/08/17.
 */
public class TesteAutomatizado {


    public static void main(String[] args) {


        System.setProperty("webdriver.chrome.driver", "/Users/herickmota/IdeaProjects/alura-selenium/drivers/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com.br");
        driver.manage().window().maximize();

        WebElement caixaDeTexto = driver.findElement(name("q"));
        caixaDeTexto.sendKeys("Caelum");
        caixaDeTexto.submit();

        driver.quit();

    }
}
